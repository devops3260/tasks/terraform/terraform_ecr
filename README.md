#### ECR repository

This is simple ECR project. In which we created a public ECR  repository in AWS and also built and pushed our container to ECR using CI

#### Steps

1. Creating ECR repository with help Terraform
2. Running CI pipeline which  building and pushing our container to ECR repository

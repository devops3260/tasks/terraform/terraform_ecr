output "repository_url" {
  value       = module.ecr.repository_url
  description = "ECR url"
}
